package com.gitlab.aliakseikaraliou.oner.base.models

interface ConversationModel {
    val itemCount
        get() = conversations.size
    
    val conversations: List<Conversation>
    
}