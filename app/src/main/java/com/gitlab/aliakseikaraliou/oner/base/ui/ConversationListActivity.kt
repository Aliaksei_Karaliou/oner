package com.gitlab.aliakseikaraliou.oner.base.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.viewpager.widget.ViewPager
import com.gitlab.aliakseikaraliou.oner.R
import com.gitlab.aliakseikaraliou.oner.base.ui.base.BaseActivity
import com.gitlab.aliakseikaraliou.oner.base.ui.viewpager.ViewPagerAdapter
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.view.SmsConversationListFragment
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.view.TelegramConversationListFragment
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.ui.view.VkConversationListFragment
import kotlinx.android.synthetic.main.activity_conversation_list.*

class ConversationListActivity : BaseActivity() {
    override val layoutId = R.layout.activity_conversation_list
    
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_sms -> viewPager.currentItem = 0
                R.id.menu_telegram -> viewPager.currentItem = 1
                R.id.menu_vk -> viewPager.currentItem = 2
            }
            
            false
        }
        
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            
            var prevMenuItem: MenuItem? = null
            
            override fun onPageSelected(position: Int) {
                
                prevMenuItem?.isChecked = false
                prevMenuItem = bottomNavigationView.menu.getItem(position)
                prevMenuItem?.isChecked = true
                
            }
        })
        
        viewPager.adapter = ViewPagerAdapter(listOf(SmsConversationListFragment(),
                TelegramConversationListFragment(),
                VkConversationListFragment()),
                supportFragmentManager)
    }
}

