package com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.ConversationModel
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.VkConversation

data class VkConversationModel(override val conversations: List<VkConversation>,
                               override val itemCount: Int) : ConversationModel
