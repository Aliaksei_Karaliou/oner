package com.gitlab.aliakseikaraliou.oner.base.utils.extension

import com.google.gson.JsonArray
import com.google.gson.JsonObject

@Suppress("IMPLICIT_CAST_TO_ANY")
inline fun <reified T> JsonObject.getData(columnName: String) = when (T::class) {
    Int::class -> get(columnName)?.asInt
    JsonArray::class -> get(columnName)?.asJsonArray
    JsonObject::class -> get(columnName)?.asJsonObject
    Long::class -> get(columnName)?.asLong
    String::class -> get(columnName)?.asString
    else -> throw IllegalArgumentException("${T::class} not supported for gson deserializing")
} as T