package com.gitlab.aliakseikaraliou.oner.vk.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gitlab.aliakseikaraliou.oner.vk.db.dao.VkChannelDao
import com.gitlab.aliakseikaraliou.oner.vk.db.dao.VkChatDao
import com.gitlab.aliakseikaraliou.oner.vk.db.dao.VkUserDao
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkChannelEntity
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkChatEntity
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkUserEntity

@Database(entities = [VkUserEntity::class, VkChatEntity::class, VkChannelEntity::class], version = 1, exportSchema = false)
abstract class VkDatabase : RoomDatabase() {
    abstract val userDao: VkUserDao
    abstract val channelDao: VkChannelDao
    abstract val chatDao: VkChatDao
}