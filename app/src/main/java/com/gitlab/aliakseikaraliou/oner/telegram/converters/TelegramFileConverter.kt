package com.gitlab.aliakseikaraliou.oner.telegram.converters

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramFile
import org.drinkless.td.libcore.telegram.TdApi

object TelegramFileConverter {
    fun convert(file: TdApi.File) = TelegramFile(file.id,
                    file.local.path)
}