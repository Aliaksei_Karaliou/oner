package com.gitlab.aliakseikaraliou.oner.base.source

class Source<out T> private constructor(val status: Status,
                                        val error: Throwable? = null,
                                        val item: T? = null) {

    enum class Status {
        SUCCESS, LOADING, ERROR
    }

    fun success() = status == Status.SUCCESS

    fun loading() = status == Status.LOADING

    fun error() = status == Status.ERROR

    companion object {
        fun <T> success(item: T) = Source(item = item,
                status = Status.SUCCESS)

        fun <T> loading() = Source<T>(Status.LOADING)

        fun <T> error(throwable: Throwable? = null) = Source<T>(error = throwable, status = Status.ERROR)
    }
}