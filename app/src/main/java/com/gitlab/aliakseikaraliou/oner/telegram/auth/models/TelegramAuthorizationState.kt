package com.gitlab.aliakseikaraliou.oner.telegram.auth.models

enum class TelegramAuthorizationState {
    ENTER_PHONE_NUMBER,
    ENTER_CODE,
    READY
}