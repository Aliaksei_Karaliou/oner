package com.gitlab.aliakseikaraliou.oner.vk.utils.exceptions

object VkErrorCodes {
    const val TOO_MANY_REQUESTS = 6
}