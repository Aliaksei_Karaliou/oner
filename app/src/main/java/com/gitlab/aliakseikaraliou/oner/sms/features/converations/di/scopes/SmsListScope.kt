package com.gitlab.aliakseikaraliou.oner.sms.features.converations.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SmsListScope