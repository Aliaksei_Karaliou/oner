package com.gitlab.aliakseikaraliou.oner.base.utils.liveData

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer

class ZippingLiveData : LiveData<ZippingLiveData.DataHolder>() {

    private val mediatorLiveData = MediatorLiveData<ZippingLiveData.DataHolder>()
    private val holder = DataHolder()
    private val received = mutableMapOf<LiveData<*>, Boolean>()

    init {
        mediatorLiveData.observeForever {}
    }

    fun <T> addSource(liveData: LiveData<T>, observer: Observer<T>? = null) {
        received[liveData] = false

        mediatorLiveData.addSource(liveData) {
            received[liveData] = true
            holder[liveData] = it

            observer?.onChanged(it)

            sendResultIfNeeded()
        }
    }

    private fun sendResultIfNeeded() {
        val loaded = received.filter { !it.value }.isEmpty()

        if (loaded) {
            postValue(holder)

            received.forEach { received[it.key] = false }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class DataHolder {
        private val data = mutableMapOf<LiveData<*>, Any?>()

        internal operator fun <T> set(liveData: LiveData<T>, value: T?) {
            data[liveData] = value
        }

        fun <T> get(liveData: LiveData<T>): T? = data[liveData] as? T?
    }
}