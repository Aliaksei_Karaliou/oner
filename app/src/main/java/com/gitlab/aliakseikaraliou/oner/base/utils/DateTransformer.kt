package com.gitlab.aliakseikaraliou.oner.base.utils

import android.content.Context
import com.gitlab.aliakseikaraliou.oner.R
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.toCalendar
import java.text.SimpleDateFormat
import java.util.*

object DateTransformer {
    private object DateFormats {
        const val yearFormat = "dd MMM yyyy"
        const val defaultFormat = "dd MMM"
        const val todayFormat = "HH:mm"
    }

    fun transform(context: Context, date: Date): String {
        val now = Date()

        val fromCalendar = date.toCalendar()
        val toCalendar = now.toCalendar()

        return when {
            fromCalendar.get(Calendar.YEAR) != toCalendar.get(Calendar.YEAR) ->
                SimpleDateFormat(DateFormats.yearFormat, Locale.getDefault()).format(date)

            fromCalendar.get(Calendar.DAY_OF_YEAR) == toCalendar.get(Calendar.DAY_OF_YEAR) ->
                SimpleDateFormat(DateFormats.todayFormat, Locale.getDefault()).format(date)

            fromCalendar.get(Calendar.DAY_OF_YEAR) + 1 == toCalendar.get(Calendar.DAY_OF_YEAR) ->
                context.getString(R.string.yesterday)

            else ->
                SimpleDateFormat(DateFormats.defaultFormat, Locale.getDefault()).format(date)
        }

    }
}
