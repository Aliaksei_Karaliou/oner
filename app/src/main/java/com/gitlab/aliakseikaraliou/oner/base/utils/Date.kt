package com.gitlab.aliakseikaraliou.oner.base.utils

import java.util.*
import java.util.concurrent.TimeUnit

fun dateFromTimestamp(timestamp: Long) = Date(timestamp * 1000)