package com.gitlab.aliakseikaraliou.oner.vk.db.repositories

import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkReceiver
import com.gitlab.aliakseikaraliou.oner.vk.utils.VkPeerer
import kotlinx.coroutines.experimental.async

class VkPeerRepository(private val userRepository: VkUserRepository,
                       private val chatRepository: VkChatRepository,
                       private val channelRepository: VkChannelRepository) {

    suspend fun loadByPeerId(ids: Collection<Long>): List<VkReceiver> {
        val receivers = mutableListOf<VkReceiver>()

        val userDeferred = async {
            val userIds = ids
                    .filter { VkPeerer.isUserId(it) }
                    .map { VkPeerer.peerIdToUserId(it) }
            userRepository.loadUsersById(userIds)
        }

        val chatDeferred = async {
            val chatIds = ids
                    .filter { VkPeerer.isChatId(it) }
                    .map { VkPeerer.peerIdToChatId(it) }
            chatRepository.loadByChatIds(chatIds)
        }

        val channelDeferred = async {
            val channelIds = ids
                    .filter { VkPeerer.isChannelId(it) }
                    .map { VkPeerer.peerIdToChannelId(it) }
            channelRepository.loadChatById(channelIds)
        }

        val users = userDeferred.await()
        receivers.addAll(users)

        val channels = channelDeferred.await()
        receivers.addAll(channels)

        val chats = chatDeferred.await()
        receivers.addAll(chats)

        return receivers
    }
}