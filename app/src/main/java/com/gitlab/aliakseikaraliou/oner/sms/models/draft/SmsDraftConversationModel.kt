package com.gitlab.aliakseikaraliou.oner.sms.models.draft

import kotlin.math.min

class SmsDraftConversationModel {
    private val map = mutableMapOf<Int, SmsDraftConversation>()

    fun put(message: SmsDraftMessage) {
        map[message.threadId]?.let { smsDraftConversation ->
            smsDraftConversation.totalCount++

            if (!message.isRead) {
                smsDraftConversation.unreadCount++
            }

            if (message.date > smsDraftConversation.lastMessage.date) {
                smsDraftConversation.lastMessage = message
            }
        } ?: run {
            map[message.threadId] = SmsDraftConversation(message, message.threadId)
        }
    }

    fun subList(offset: Int, itemCount: Int): List<SmsDraftConversation> {
        val to = min(offset + itemCount - 1, map.size)

        if (offset >= map.size) {
            return listOf()
        }

        return map.map {
            it.value
        }.sortedByDescending {
            it.lastMessage.date
        }.subList(offset, to)
    }
}