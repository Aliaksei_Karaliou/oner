package com.gitlab.aliakseikaraliou.oner.telegram.converters

import android.text.format.DateUtils
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.TelegramDraftMessage
import org.drinkless.td.libcore.telegram.TdApi
import java.util.Date

object TelegramMessageConverter {
    fun convert(value: TdApi.Message?, type: TdApi.ChatType): TelegramDraftMessage? {
        return value?.let { message ->
            val text: String? = if (message.content is TdApi.MessageText) {
                (message.content as TdApi.MessageText).text.text
            } else {
                null
            }

            var authorId = 0L
            var chatId = 0L

            when (type) {
                is TdApi.ChatTypePrivate -> {
                    authorId = value.chatId
                    chatId = 0
                }
                is TdApi.ChatTypeBasicGroup -> {
                    authorId = value.senderUserId.toLong()
                    chatId = value.chatId
                }
                is TdApi.ChatTypeSupergroup -> {
                    if (type.isChannel) {
                        authorId = value.chatId
                        chatId = 0
                    } else {
                        authorId = value.senderUserId.toLong()
                        chatId = value.chatId
                    }
                }
            }

            TelegramDraftMessage(id = message.id,
                    signature = message.authorSignature,
                    text = text,
                    authorId = authorId,
                    date = Date(message.date * DateUtils.SECOND_IN_MILLIS),
                    chatId = chatId,
                    isRead = true,
                    isOut = message.isOutgoing)
        }
    }
}