package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.di.components

import com.gitlab.aliakseikaraliou.oner.base.di.components.AppComponent
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.di.scopes.ConversationListScope
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.di.components.SmsListComponent
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.di.components.TelegramListComponent
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.di.components.VkListComponent
import dagger.Component

@ConversationListScope
@Component(dependencies = [AppComponent::class])
interface ConversationListComponent {
    fun telegram(): TelegramListComponent.Builder
    
    fun sms(): SmsListComponent.Builder
    
    fun vk(): VkListComponent.Builder
}