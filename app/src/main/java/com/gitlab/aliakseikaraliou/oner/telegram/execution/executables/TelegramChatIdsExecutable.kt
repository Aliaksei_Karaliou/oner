package com.gitlab.aliakseikaraliou.oner.telegram.execution.executables

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramChatKey
import org.drinkless.td.libcore.telegram.TdApi

class TelegramChatIdsExecutable(count: Int,
                                chatKey: TelegramChatKey) : BaseTelegramExecutable<TdApi.Chats, List<Long>>() {
    override val function = TdApi.GetChats(chatKey.chatOrder, chatKey.chatId, count)

    override fun convertData(it: TdApi.Chats) = it.chatIds.toList()
}
