package com.gitlab.aliakseikaraliou.oner.vk.features.conversations.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.dataSource.VkConversationModelDataSourceFactory

@Suppress("UNCHECKED_CAST")
class VkConversationListViewModelFactory(private val dataSource: VkConversationModelDataSourceFactory) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>) = when (modelClass) {
        VkConversationListViewModel::class.java -> VkConversationListViewModel(dataSource)
        else -> throw IllegalArgumentException()
    } as T
}