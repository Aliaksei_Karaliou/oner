package com.gitlab.aliakseikaraliou.oner.telegram.db.repositories

import com.gitlab.aliakseikaraliou.oner.telegram.execution.executables.TelegramChannelExecutable
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChannel
import kotlinx.coroutines.experimental.async

class TelegramChannelRepository(private val downloadFileRepository: TelegramDownloadFileRepository) {
    suspend fun loadChannelById(id: Long): TelegramChannel {
        val executable = TelegramChannelExecutable(id)
        val draftChannel = executable.execute()

        return when {
            draftChannel.imageUrl != null -> {
                draftChannel.build()
            }
            draftChannel.imageId != null -> {
                val async = async {
                    downloadFileRepository.downloadFile(draftChannel.imageId)
                }

                draftChannel.build(async.await().localPath)
            }
            else -> {
                draftChannel.build()
            }
        }
    }

}