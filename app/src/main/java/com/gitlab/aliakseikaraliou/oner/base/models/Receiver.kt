package com.gitlab.aliakseikaraliou.oner.base.models

interface Receiver {
    val id: Long
    val fullName: String
    val imageUrl: String?
}