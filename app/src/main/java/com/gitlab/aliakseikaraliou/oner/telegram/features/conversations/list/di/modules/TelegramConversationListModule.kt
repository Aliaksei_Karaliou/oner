package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.di.modules

import androidx.lifecycle.ViewModelProviders
import com.gitlab.aliakseikaraliou.oner.base.db.BaseRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.telegram.db.repositories.TelegramConversationModelRepository
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.dataSource.TelegramConversationModelDataSourceFactory
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.di.scopes.TelegramListScope
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.view.TelegramConversationListFragment
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.viewModel.TelegramConversationListViewModel
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.viewModel.TelegramConversationListViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class TelegramConversationListModule {

    @TelegramListScope
    @Provides
    fun provideRepository(database: BaseRepositoryFactory) = database.telegram.conversationModelRepository
    
    @TelegramListScope
    @Provides
    fun provideViewModelFactory(repository: TelegramConversationModelRepository) = TelegramConversationModelDataSourceFactory(repository)

    @TelegramListScope
    @Provides
    fun provideViewModel(fragment: TelegramConversationListFragment,
                         dataSourceFactory: TelegramConversationModelDataSourceFactory)
            : ConversationListPagedViewModel {
        val viewModelFactory = TelegramConversationListViewModelFactory(dataSourceFactory)

        return ViewModelProviders.of(fragment, viewModelFactory)
                .get(TelegramConversationListViewModel::class.java)
    }
}