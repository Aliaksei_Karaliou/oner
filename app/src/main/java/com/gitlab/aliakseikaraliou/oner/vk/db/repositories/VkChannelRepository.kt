package com.gitlab.aliakseikaraliou.oner.vk.db.repositories

import com.gitlab.aliakseikaraliou.oner.vk.db.dao.VkChannelDao
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkChannelEntity
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkPeerCache
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChannel
import com.gitlab.aliakseikaraliou.oner.vk.network.VkApi

class VkChannelRepository(private val channelDao: VkChannelDao,
                          private val api: VkApi,
                          private val peerCache: VkPeerCache) {

    suspend fun loadChatById(ids: List<Long>): List<VkChannel> {
        val channels = mutableListOf<VkChannel>()

        if (ids.isEmpty()) {
            return channels
        }

        val freeIds = mutableListOf<Long>()

        ids.forEach { id ->
            (peerCache[id] as? VkChannel)?.let {
                channels.add(it)
            } ?: run {
                freeIds.add(id)
            }
        }

        if (freeIds.isEmpty()) {
            return channels
        }

        val loaded = api.getChannels(ids.joinToString(separator = ","))
                .execute()
                .takeIf { it.isSuccessful }
                ?.body()
                ?.list
                ?.also { loadedChannels ->
                    peerCache.putAll(loadedChannels)
                    channelDao.insert(loadedChannels.map { VkChannelEntity(it) })
                } ?: listOf()
        channels.addAll(loaded)

        return channels
    }
}