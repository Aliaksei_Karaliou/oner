package com.gitlab.aliakseikaraliou.oner.telegram.converters

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChannel
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors.TelegramDraftChannel
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors.TelegramDraftChat
import org.drinkless.td.libcore.telegram.TdApi

object TelegramChannelConverter {
    fun convert(chat: TdApi.Chat): TelegramDraftChannel {
        val photoId = chat.photo?.small?.id?.takeIf { it > 0 }
        val localPath = chat.photo?.small?.local?.path?.takeIf { it.isNotEmpty() }

        return TelegramDraftChannel(id = chat.id,
                fullName = chat.title,
                imageId = photoId,
                imageUrl = localPath)
    }
}