package com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Receiver

interface VkReceiver : Receiver {
    val peerId: Long
}