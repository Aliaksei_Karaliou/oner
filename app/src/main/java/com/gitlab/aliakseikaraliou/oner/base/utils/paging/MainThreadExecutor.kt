package com.gitlab.aliakseikaraliou.oner.base.utils.paging

import android.os.Handler
import java.util.concurrent.Executor

class MainThreadExecutor : Executor {
    private val handled = Handler()

    override fun execute(command: Runnable?) {
        command?.let { handled.post(it) }
    }
}