package com.gitlab.aliakseikaraliou.oner.vk.models.clean

import java.util.*

@Suppress("DataClassPrivateConstructor")
data class VkOnlineStatus private constructor(val status: Status, val date: Date? = null) {
    enum class Status {
        WEB, MOBILE, OFFLINE
    }

    fun isWeb() = status == Status.WEB

    fun isMobile() = status == Status.MOBILE

    companion object {
        fun web() = VkOnlineStatus(Status.WEB)
        fun mobile() = VkOnlineStatus(Status.MOBILE)
        fun offline(date: Date? = null) = VkOnlineStatus(Status.OFFLINE, date)
    }

}