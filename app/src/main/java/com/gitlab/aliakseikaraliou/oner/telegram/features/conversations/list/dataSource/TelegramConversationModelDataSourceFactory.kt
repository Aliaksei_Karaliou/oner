package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.dataSource

import androidx.paging.DataSource
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.telegram.db.repositories.TelegramConversationModelRepository
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramChatKey

class TelegramConversationModelDataSourceFactory(private val repository: TelegramConversationModelRepository)
    : DataSource.Factory<TelegramChatKey, Conversation>() {
    
    override fun create() = TelegramConversationModelDataSource(repository)
}