package com.gitlab.aliakseikaraliou.oner.telegram.auth.di.components

import com.gitlab.aliakseikaraliou.oner.telegram.auth.di.modules.TelegramAuthorizationModule
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.TelegramAuthorizationActivity
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments.BaseTelegramAuthorizationFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [TelegramAuthorizationModule::class])

interface TelegramAuthorizationComponent {
    fun inject(activity: TelegramAuthorizationActivity)

    fun inject(fragment: BaseTelegramAuthorizationFragment)
}