package com.gitlab.aliakseikaraliou.oner.vk.models.draft

data class VkDraftConversationModel(val count: Int,
                                    val conversations: List<VkDraftConversation>)