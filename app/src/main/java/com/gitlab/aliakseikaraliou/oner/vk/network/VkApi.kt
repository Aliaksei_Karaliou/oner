package com.gitlab.aliakseikaraliou.oner.vk.network

import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkChannelList
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkChatList
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkUserList
import com.gitlab.aliakseikaraliou.oner.vk.models.draft.VkDraftConversationModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface VkApi {
    @GET("messages.getConversations")
    fun getConversations(@Query("offset") offset: Int,
                         @Query("count") count: Int): Call<VkDraftConversationModel>
    
    @GET("users.get")
    fun getUsers(@Query("user_ids") ids: String,
                 @Query("fields") fields: String): Call<VkUserList>

    
    @GET("messages.getChat")
    fun getChats(@Query("chat_ids") ids: String): Call<VkChatList>
    
    @GET("groups.getById")
    fun getChannels(@Query("group_ids") ids: String): Call<VkChannelList>
    
}
