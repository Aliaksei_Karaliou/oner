package com.gitlab.aliakseikaraliou.oner.vk.network.deserializers

import com.gitlab.aliakseikaraliou.oner.base.utils.extension.getData
import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkChannelList
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChannel
import com.google.gson.JsonElement

class VkChannelDeserializer : BaseVkDeserializer<VkChannelList>() {
    override fun deserialize(json: JsonElement): VkChannelList {
        val list = json.asJsonArray.map {
            val jsonObject = it.asJsonObject
            
            val id = jsonObject.getData<Long>(VkConstants.Json.ID)
            val name = jsonObject.getData<String>(VkConstants.Json.NAME)
            val photo50 = jsonObject.getData<String?>(VkConstants.Json.PHOTO_100)
            
            VkChannel(id = id,
                    fullName = name,
                    imageUrl = photo50)
        }
        
        return VkChannelList(list)
    }
    
}