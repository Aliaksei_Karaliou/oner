package com.gitlab.aliakseikaraliou.oner.base.utils.extension

import androidx.databinding.BindingAdapter
import androidx.core.content.ContextCompat
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.gitlab.aliakseikaraliou.oner.R
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.base.models.User
import com.gitlab.aliakseikaraliou.oner.base.utils.DateTransformer
import java.util.Date

@BindingAdapter("glideRound", "textPlaceHolder")
internal fun ImageView.glideRound(imageUrl: String?, textPlaceHolder: String? = null) {
    val drawable = textPlaceHolder?.let {
        TextDrawable.builder()
                .buildRound(it, ColorGenerator.MATERIAL.getColor(textPlaceHolder))
    }

    val options = RequestOptions()
            .circleCrop()
            .placeholder(drawable)
            .error(drawable)
            .diskCacheStrategy(DiskCacheStrategy.ALL)

    Glide.with(this)
            .load(imageUrl)
            .apply(options)
            .into(this)
}

@BindingAdapter("date")
internal fun TextView.date(date: Date?) {
    text = if (date == null) String.EMPTY else DateTransformer.transform(context, date)
}

@BindingAdapter("visible")
internal fun View.visible(visibility: Boolean?) {
    isVisible = visibility ?: false
}

@BindingAdapter("onlineStatus")
internal fun ImageView.onlineStatus(conversation: Conversation) {
    val onlineWeb = (conversation.receiver as? User)?.isOnlineWeb ?: false
    val onlineMobile = (conversation.receiver as? User)?.isOnlineMobile ?: false

    when {
        onlineWeb -> {
            isVisible = true
            setImageResource(R.drawable.ic_circle)
        }
        onlineMobile -> {
            isVisible = true
            setImageResource(R.drawable.ic_mobile)
        }
        else -> {
            isVisible = false
        }
    }
}

@BindingAdapter("imageText")
internal fun ImageView.imageText(text: String?) {
    text.orEmpty()
            .also {
                val drawable = TextDrawable.builder()
                        .buildRound(it, ContextCompat.getColor(context, R.color.white))
                setImageDrawable(drawable)
                requestLayout()
            }
}
