package com.gitlab.aliakseikaraliou.oner.telegram.execution.executables

import com.gitlab.aliakseikaraliou.oner.telegram.converters.TelegramUserConverter
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramUser
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors.TelegramDraftUser
import org.drinkless.td.libcore.telegram.TdApi

class TelegramUserExecutable(userId: Long) : BaseTelegramExecutable<TdApi.User, TelegramDraftUser>() {
    override val function = TdApi.GetUser(userId.toInt())
    
    override fun convertData (it: TdApi.User) = TelegramUserConverter.convert(it)
}
