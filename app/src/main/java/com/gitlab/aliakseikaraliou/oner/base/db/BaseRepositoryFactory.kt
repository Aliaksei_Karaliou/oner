package com.gitlab.aliakseikaraliou.oner.base.db

import com.gitlab.aliakseikaraliou.oner.sms.db.SmsRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.telegram.db.TelegramRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.VkRepositoryFactory

data class BaseRepositoryFactory(val telegram: TelegramRepositoryFactory,
                                 val sms: SmsRepositoryFactory,
                                 val vk: VkRepositoryFactory)