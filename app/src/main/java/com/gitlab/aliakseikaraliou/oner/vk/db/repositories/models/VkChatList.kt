package com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models

import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChat

data class VkChatList(val list: List<VkChat>)