package com.gitlab.aliakseikaraliou.oner.base

import android.app.Application
import android.content.Context
import com.gitlab.aliakseikaraliou.oner.base.di.components.AppComponent
import com.gitlab.aliakseikaraliou.oner.base.di.components.DaggerAppComponent
import com.gitlab.aliakseikaraliou.oner.base.di.modules.ContextModule
import com.gitlab.aliakseikaraliou.oner.base.utils.LibUtils

class OnerApplication : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        LibUtils.init(this)

        OnerConfiguration.applicationDir = applicationInfo.dataDir

        appComponent = DaggerAppComponent.builder()
                .contextModule(ContextModule(this))
                .build()
    }

    companion object {
        fun get(context: Context): OnerApplication {
            return context.applicationContext as OnerApplication
        }
    }

}