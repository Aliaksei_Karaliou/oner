package com.gitlab.aliakseikaraliou.oner.vk.network.deserializers

import com.gitlab.aliakseikaraliou.oner.base.utils.extension.getData
import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkChatList
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChat
import com.google.gson.JsonElement

class VkChatListDeserializer : BaseVkDeserializer<VkChatList>() {
    override fun deserialize(json: JsonElement): VkChatList {
        
        val list = json.asJsonArray.map { element ->
            val jsonObject = element.asJsonObject
            
            val id = jsonObject.getData<Long>(VkConstants.Json.ID)
            val title = jsonObject.getData<String>(VkConstants.Json.TITLE)
            val photo50 = jsonObject.getData<String?>(VkConstants.Json.PHOTO_100)
            
            VkChat(id = id,
                    fullName = title,
                    imageUrl = photo50)
        }
        return VkChatList(list)
    }
    
}