package com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models

import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkReceiver
import java.util.*
import java.util.concurrent.TimeUnit

class VkPeerCache{
    private val cacheTime = TimeUnit.MINUTES.toMillis(CACHE_TIME_MINUTES.toLong())
    private val map = mutableMapOf<Long, Pair<VkReceiver, Long>>()

    fun putAll(receivers: List<VkReceiver>) {
        val date = Date()
        receivers.forEach { put(it, date) }
    }

    fun contains(peerId: Long) = get(peerId) != null

    private fun put(receiver: VkReceiver, time: Date) {
        map[receiver.peerId] = Pair(receiver, time.time)
    }

    operator fun get(receiver: Long) = map[receiver]
            ?.takeIf { Date().time < it.second + cacheTime }
            ?.first

    companion object {
        private const val CACHE_TIME_MINUTES = 3
    }
}