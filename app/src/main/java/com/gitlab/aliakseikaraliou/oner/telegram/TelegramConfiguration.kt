package com.gitlab.aliakseikaraliou.oner.telegram

object TelegramConfiguration {
    const val apiId = 216347
    const val hashId = "08396f297f4532a121ea784e32d118b4"
    
    const val INITIAL_PAGE_SIZE = 100
    const val PAGE_SIZE = 20
}