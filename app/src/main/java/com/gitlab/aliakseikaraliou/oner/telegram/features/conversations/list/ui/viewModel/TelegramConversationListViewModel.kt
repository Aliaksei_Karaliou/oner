package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.viewModel

import androidx.paging.PagedList
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.telegram.TelegramConfiguration
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.dataSource.TelegramConversationModelDataSourceFactory

class TelegramConversationListViewModel(
        dataSourceFactory: TelegramConversationModelDataSourceFactory) : ConversationListPagedViewModel(dataSourceFactory) {
    
    override val config: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(TelegramConfiguration.INITIAL_PAGE_SIZE)
            .setPageSize(TelegramConfiguration.PAGE_SIZE)
            .build()
}
