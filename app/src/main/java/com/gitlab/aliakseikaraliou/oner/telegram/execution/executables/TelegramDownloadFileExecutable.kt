package com.gitlab.aliakseikaraliou.oner.telegram.execution.executables

import com.gitlab.aliakseikaraliou.oner.telegram.converters.TelegramFileConverter
import com.gitlab.aliakseikaraliou.oner.telegram.models.TelegramPriority
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramFile
import org.drinkless.td.libcore.telegram.TdApi

class TelegramDownloadFileExecutable(private val fileId: Int,
                                     private val priority: TelegramPriority)
    : BaseTelegramExecutable<TdApi.File, TelegramFile>() {
    override val function: TdApi.Function
        get() = TdApi.DownloadFile(fileId, priority.value)
    
    override fun convertData(it: TdApi.File) = TelegramFileConverter.convert(it)
}