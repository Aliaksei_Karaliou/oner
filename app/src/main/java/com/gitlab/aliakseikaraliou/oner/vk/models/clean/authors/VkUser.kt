package com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.vk.models.clean.VkOnlineStatus
import com.gitlab.aliakseikaraliou.oner.base.models.User
import com.gitlab.aliakseikaraliou.oner.vk.utils.VkPeerer

data class VkUser(override val firstName: String,
                  override val lastName: String,
                  override val username: String?,
                  override val id: Long,
                  override val imageUrl: String?,
                  val onlineStatus: VkOnlineStatus) : User, VkAuthor {
    override val isOnlineMobile = onlineStatus.isMobile()
    override val isOnlineWeb = onlineStatus.isWeb()

    override val peerId = VkPeerer.userIdToPeerId(id)
}