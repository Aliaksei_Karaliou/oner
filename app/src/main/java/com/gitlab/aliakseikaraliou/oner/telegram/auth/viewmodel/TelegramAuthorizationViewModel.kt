package com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel

import androidx.lifecycle.ViewModel
import com.gitlab.aliakseikaraliou.oner.telegram.auth.models.TelegramAuthorizationState
import com.gitlab.aliakseikaraliou.oner.telegram.auth.models.TelegramAuthorizator
import com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.navigators.TelegramAuthorizationNavigator

class TelegramAuthorizationViewModel(val authorizator: TelegramAuthorizator) : ViewModel() {
    var navigator: TelegramAuthorizationNavigator? = null

    init {
        authorizator.onAuthorizationStateListener = {
            when (it) {
                TelegramAuthorizationState.ENTER_PHONE_NUMBER -> openPhoneFragment()
                TelegramAuthorizationState.ENTER_CODE -> openCodeFragment()
                TelegramAuthorizationState.READY -> onReady()
            }
        }
    }

    fun initializeAuthorizator() {
        authorizator.initialize()
    }

    private fun openPhoneFragment() {
        navigator?.openPhoneFragment()
    }

    private fun openCodeFragment() {
        navigator?.openCodeFragment()
    }

    private fun onReady() {
        navigator?.openMainActivity()
    }

}