package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ConversationListScope