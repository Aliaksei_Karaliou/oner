package com.gitlab.aliakseikaraliou.oner.vk.features.conversations.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class VkListScope