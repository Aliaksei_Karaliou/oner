package com.gitlab.aliakseikaraliou.oner.sms.features.converations.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PositionalDataSource
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.sms.db.repositories.SmsConversationModelRepository
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.SmsConversationModel
import kotlinx.coroutines.experimental.launch

class SmsConversationModelDataSource(val repository: SmsConversationModelRepository) : PositionalDataSource<Conversation>() {

    val liveData = MutableLiveData<Source<SmsConversationModel>>()

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Conversation>) {
        liveData.postValue(Source.loading())

        launch {
            val conversationModel = repository.loadConversations(offset = params.requestedStartPosition,
                    count = params.requestedLoadSize)

            liveData.postValue(conversationModel)
            conversationModel.item?.let {
                callback.onResult(it.conversations, params.requestedStartPosition)
            }
        }
    }

    override fun loadRange(params: PositionalDataSource.LoadRangeParams, callback: PositionalDataSource.LoadRangeCallback<Conversation>) {
        launch {
            val conversationModel = repository.loadConversations(offset = params.startPosition,
                    count = params.loadSize)

            liveData.postValue(conversationModel)
            conversationModel.item?.conversations?.let {
                callback.onResult(it)
            }
        }
    }
}