package com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Chat
import com.gitlab.aliakseikaraliou.oner.vk.utils.VkPeerer

data class VkChat(override val id: Long,
                  override val fullName: String,
                  override val imageUrl: String?) : Chat, VkReceiver {
    override val peerId = VkPeerer.chatIdToPeerId(id)
}