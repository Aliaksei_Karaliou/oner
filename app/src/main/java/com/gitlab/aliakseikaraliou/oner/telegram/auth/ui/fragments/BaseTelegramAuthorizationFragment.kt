package com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.gitlab.aliakseikaraliou.oner.base.ui.base.BaseFragment
import com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel.TelegramAuthorizationViewModel
import com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel.factory.TelegramAuthorizationViewModelFactory
import javax.inject.Inject

abstract class BaseTelegramAuthorizationFragment : BaseFragment() {
    lateinit var viewModel: TelegramAuthorizationViewModel

    @Inject
    protected lateinit var viewModelFactory: TelegramAuthorizationViewModelFactory

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(TelegramAuthorizationViewModel::class.java)
    }
}