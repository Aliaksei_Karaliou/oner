package com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChat
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramUser
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramChatConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.TelegramDraftMessage

data class TelegramChatDraftConversation(val chatId: Long,
                                         val userId: Long?,
                                         val isPinned: Boolean,
                                         val lastMessage: TelegramDraftMessage?,
                                         val unreadCount: Int,
                                         val order: Long)
    : TelegramDraftConversation {

    fun build(chat: TelegramChat, author: TelegramUser? = null): TelegramChatConversation {
        if (author?.id != userId || chat.id != chatId) {
            throw IllegalArgumentException()
        }

        val message = lastMessage?.convert(author = author, chat = chat)

        return TelegramChatConversation(receiver = chat,
                lastMessage = message,
                isPinned = isPinned,
                unreadCount = unreadCount,
                order = order)
    }
}