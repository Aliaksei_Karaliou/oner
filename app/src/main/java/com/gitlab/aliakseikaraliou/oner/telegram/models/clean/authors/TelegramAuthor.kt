package com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Author

interface TelegramAuthor : Author, TelegramReceiver