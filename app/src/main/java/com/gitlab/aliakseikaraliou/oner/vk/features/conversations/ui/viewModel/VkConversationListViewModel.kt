package com.gitlab.aliakseikaraliou.oner.vk.features.conversations.ui.viewModel

import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.vk.VkConfiguration
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.dataSource.VkConversationModelDataSourceFactory
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkConversationModel

class VkConversationListViewModel(dataSourceFactory: VkConversationModelDataSourceFactory) : ConversationListPagedViewModel(dataSourceFactory) {
    override val config: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(VkConfiguration.INITIAL_PAGE_SIZE)
            .setPageSize(VkConfiguration.PAGE_SIZE)
            .build()

    private val progressLiveData = dataSourceFactory.create().liveData
    private val observer = Observer<Source<VkConversationModel>> { presentationalModel ->
        presentationalModel?.let { liveData.postValue(it) }
    }

    override fun loadConversations() {
        liveData.addSource(progressLiveData, observer)

        super.loadConversations()
    }

    override fun onCleared() {
        liveData.removeSource(progressLiveData)

        super.onCleared()
    }
}