package com.gitlab.aliakseikaraliou.oner.base.models

import java.util.Date

interface Message {
    val id: Long
    val text: String?
    val author: Author?
    val chat: Chat?
    val date: Date
    val isRead: Boolean
    val isOut: Boolean
}