package com.gitlab.aliakseikaraliou.oner.vk

import com.gitlab.aliakseikaraliou.oner.vk.db.VkDatabase
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkPeerCache
import com.gitlab.aliakseikaraliou.oner.vk.network.VkApi

class BaseVkModel(val api: VkApi,
                  val database: VkDatabase,
                  val peerCache: VkPeerCache)
