package com.gitlab.aliakseikaraliou.oner.vk.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkUserEntity

@Suppress("unused")
@Dao
interface VkUserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: VkUserEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<VkUserEntity>)
}