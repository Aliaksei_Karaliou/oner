package com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramMessage
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChat

data class TelegramChatConversation(override val receiver: TelegramChat,
                                    override val lastMessage: TelegramMessage?,
                                    override val isPinned: Boolean,
                                    override val unreadCount: Int,
                                    override val order: Long) : TelegramConversation