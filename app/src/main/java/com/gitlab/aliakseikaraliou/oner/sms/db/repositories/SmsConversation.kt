package com.gitlab.aliakseikaraliou.oner.sms.db.repositories

import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.SmsMessage
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors.SmsAuthor

data class SmsConversation(override val receiver: SmsAuthor,
                           override val lastMessage: SmsMessage,
                           val threadId: Int,
                           override val unreadCount: Int,
                           val totalCount: Int) : Conversation {
    override val isPinned = false
}
