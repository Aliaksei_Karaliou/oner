package com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramUser
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramPrivateConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.TelegramDraftMessage

data class TelegramPrivateDraftConversation(val authorId: Long,
                                            val draftMessage: TelegramDraftMessage?,
                                            val isPinned: Boolean,
                                            val isSecret: Boolean,
                                            val unreadCount: Int,
                                            val order: Long) : TelegramDraftConversation {

    fun convert(author: TelegramUser): TelegramPrivateConversation {
        if (author.id != authorId) {
            throw IllegalArgumentException()
        }

        val message = draftMessage?.convert(author)

        return TelegramPrivateConversation(receiver = author,
                lastMessage = message,
                isSecret = isSecret,
                isPinned = isPinned,
                unreadCount = unreadCount,
                order = order)
    }
}